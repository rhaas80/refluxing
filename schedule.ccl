# Schedule definitions for thorn Refluxing

if (refluxing_debug_variables) {
  STORAGE: restrict_weight_fine
  STORAGE: restrict_weight_coarse
  STORAGE: flux_weight_fine
  STORAGE: flux_weight_coarse
}

STORAGE: flux
STORAGE: register_fine[2]
STORAGE: register_coarse[2]
STORAGE: correction

if (refluxing_debug_variables) {
  STORAGE: correction_total
}

SCHEDULE Refluxing_ParamCheck AT paramcheck
{
  LANG: C
} "Check parameters"

SCHEDULE Refluxing_Init AT basegrid
{
  LANG: Fortran
} "Initialise refluxing variables"

SCHEDULE Refluxing_Init2 AT basegrid
{
  LANG: C
  OPTIONS: global
} "Initialise internal refluxing variables"

SCHEDULE Refluxing_Init2 AT POST_RECOVER_VARIABLES
{
  LANG: C
  OPTIONS: global
} "Initialise internal refluxing variables after recovering grid structure from checkpoint"

SCHEDULE Refluxing_Reset AT postregrid
{
  LANG: C
  OPTIONS: level
} "Reset refluxing variables after regridding"

SCHEDULE Refluxing_Output AT postregrid AFTER Refluxing_Reset
{
  LANG: C
  OPTIONS: level
} "Output debug variables"

# The user should schedule the group RefluxingSyncGroup when the fluxes need to be syncronized
SCHEDULE Refluxing_Sync IN RefluxingSyncGroup
{
  LANG: C
  SYNC: flux
} "Dummy routine for sync"

# Schedule refluxing before MoL_PostStep, because MoL_PostStep calls
# con2prim, which has to happen after refluxing
# TODO: Schedule all boundary conditions etc. after this routine
if (refluxing_debug_variables) {
  SCHEDULE Refluxing_CorrectState AT postrestrict BEFORE MoL_PostStep
  {
    LANG: C
    OPTIONS: level
    STORAGE: correction

    SYNC: restrict_weight_fine
    SYNC: restrict_weight_coarse
    SYNC: flux_weight_fine
    SYNC: flux_weight_coarse
    SYNC: correction
    SYNC: correction_total
  } "Correct coarse grid state"
} else {
  SCHEDULE Refluxing_CorrectState AT postrestrict BEFORE MoL_PostStep
  {
    LANG: C
    OPTIONS: level
    STORAGE: correction
  } "Correct coarse grid state"
}
