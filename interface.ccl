# Interface definition for thorn Refluxing

IMPLEMENTS: Refluxing

USES INCLUDE HEADER: loopcontrol.h

USES INCLUDE HEADER: defs.hh
USES INCLUDE HEADER: dh.hh
USES INCLUDE HEADER: gh.hh
USES INCLUDE HEADER: operator_prototypes_3d.hh
USES INCLUDE HEADER: vect.hh

USES INCLUDE HEADER: carpet.hh

# Register variables to be refluxed, returns
CCTK_INT FUNCTION                     \
    RefluxingRegisterVariable         \
        (CCTK_INT IN VariableIndex,   \
         CCTK_INT IN UseSlowSector,   \
         CCTK_INT OUT FluxIndex)
PROVIDES FUNCTION RefluxingRegisterVariable \
    WITH Refluxing_RegisterVariable         \
    LANGUAGE C

# Register evolved variables
CCTK_INT FUNCTION                  \
    MoLRegisterEvolved             \
        (CCTK_INT IN EvolvedIndex, \
         CCTK_INT IN RHSIndex)
REQUIRES FUNCTION MoLRegisterEvolved

CCTK_INT FUNCTION                  \
    MoLRegisterEvolvedSlow         \
        (CCTK_INT IN EvolvedIndex, \
         CCTK_INT IN RHSIndex)
USES FUNCTION MoLRegisterEvolvedSlow

CCTK_INT FUNCTION MoLQueryEvolvedRHS(CCTK_INT IN EvolvedIndex)
USES FUNCTION MoLQueryEvolvedRHS

# Get pointer to grid variable for a specific map and refinement level
CCTK_POINTER FUNCTION                     \
    VarDataPtrI                           \
        (CCTK_POINTER_TO_CONST IN cctkGH, \
         CCTK_INT IN map,                 \
         CCTK_INT IN reflevel,            \
         CCTK_INT IN component,           \
         CCTK_INT IN timelevel,           \
         CCTK_INT IN varindex)
REQUIRES FUNCTION VarDataPtrI

# Get current refinement level and number of refinement levels
CCTK_INT FUNCTION                         \
    GetRegriddingEpoch                    \
        (CCTK_POINTER_TO_CONST IN cctkGH)
REQUIRES FUNCTION GetRegriddingEpoch

CCTK_INT FUNCTION                         \
    GetRegriddingEpochs                   \
        (CCTK_POINTER_TO_CONST IN cctkGH, \
         CCTK_INT IN size,                \
         CCTK_INT ARRAY OUT epochs)
REQUIRES FUNCTION GetRegriddingEpochs

CCTK_INT FUNCTION                         \
    GetRefinementLevel                    \
        (CCTK_POINTER_TO_CONST IN cctkGH)
REQUIRES FUNCTION GetRefinementLevel

CCTK_INT FUNCTION                         \
    GetRefinementLevels                   \
        (CCTK_POINTER_TO_CONST IN cctkGH)
REQUIRES FUNCTION GetRefinementLevels

# Convenient way to determine boundary sizes
CCTK_INT FUNCTION GetBoundarySizesAndTypes \
  (CCTK_POINTER_TO_CONST IN cctkGH, \
   CCTK_INT IN size, \
   CCTK_INT OUT ARRAY bndsize, \
   CCTK_INT OUT ARRAY is_ghostbnd, \
   CCTK_INT OUT ARRAY is_symbnd, \
   CCTK_INT OUT ARRAY is_physbnd)
REQUIRES FUNCTION GetBoundarySizesAndTypes

PRIVATE:

# Flux weight functions (shows where refluxing happens; for debugging only)
CCTK_REAL restrict_weight_fine TYPE=gf TAGS='Prolongation="none" Checkpoint="no" tensortypealias="D" tensorweight=+1.0' "Fine grid restriction weight"
CCTK_REAL restrict_weight_coarse TYPE=gf TAGS='Prolongation="none" Checkpoint="no" tensortypealias="D" tensorweight=+1.0' "Coarse grid restriction weight"
CCTK_REAL flux_weight_fine[3] TYPE=gf TAGS='Prolongation="none" Checkpoint="no" tensortypealias="D" tensorweight=+1.0' "Fine grid refluxing weight"
CCTK_REAL flux_weight_coarse[3] TYPE=gf TAGS='Prolongation="none" Checkpoint="no" tensortypealias="D" tensorweight=+1.0' "Coarse grid refluxing weight"



# Flux registers, where the fluxes are integrated in time
#    There are separate registers for each direction
#    There are separeate registers for coarse and fine grids,
#       and their differences determines the correction
# Stored fluxes, just capturing what GRHydro calculates
#    These are used as RHS for the flux registers
# Correction terms
#    These are temporary variables, only used
#       to calculate the difference between the coarse and fine grid registers
# Time-summed corrections, just for debugging
# All these variables exist for all conserved quantites

# There are:
#   (refluxing_nvars) conserved quantities
#   3 directions
#   fluxes, coarse and fine registers with 2 time levels each
#      make altogether for 5 * 3 * (1 + 2 + 2) = 75 additional time levels
#   refluxing is expensive memory-wise;
#      this could be optimised since these need only be stored
#      on the refinement boundaries, not in the whole volume

PUBLIC:

CCTK_REAL flux[3*refluxing_nvars] TYPE=gf TAGS='Prolongation="none" Checkpoint="no" tensortypealias="D" tensorweight=+1.0' "Captured fluxes"

PRIVATE:

CCTK_REAL register_fine[3*refluxing_nvars] TYPE=gf TIMELEVELS=2 TAGS='Prolongation="none" tensortypealias="D" tensorweight=+1.0' "Registers for fine grid fluxes"

CCTK_REAL register_coarse[3*refluxing_nvars] TYPE=gf TIMELEVELS=2 TAGS='Prolongation="none" tensortypealias="D" tensorweight=+1.0' "Registers for coarse grid fluxes"

CCTK_REAL correction[3*refluxing_nvars] TYPE=gf TAGS='Prolongation="restrict" Checkpoint="no" tensortypealias="D" tensorweight=+1.0' "Corrections"

CCTK_REAL correction_total[3*refluxing_nvars] TYPE=gf TAGS='Prolongation="none" tensortypealias="D" tensorweight=+1.0' "Accumulated corrections (for statistics only)"
